{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h1 align=\"center\">Tea Time Python Plotting Intro</h1>\n",
    "License: [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)  \n",
    "Python has a wide array of plotting capabilities. Just about everything that can be plotted with other tools can be plotted using python. Very rarely I come across something that cannot be plotted using python, such as the pairs plotting in ggplot. \n",
    "\n",
    "Here is a useful reference link to a large number of examples of pretty much everything you can do in python. The code used to make the plots is included.  \n",
    "http://matplotlib.org/gallery.html\n",
    "\n",
    "Let's start by loading the packages we will need. It is often a good idea to dedicate the first code cell to initializing the correct environment. **matplotlib** is the plotting package in python that is designed to be similar to MATLAB. Pyplot is the interface to the matplotlib plotting library. We will also use **numpy** to quickly make the data for us to plot. **%matplotlib inline** is an iPython command that displays the plots right below the output for the entire notebook. Alternatively, **plt.show()** can be used for each plot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import matplotlib\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic Plotting\n",
    "\n",
    "Let's start with a simple plot of a sine in python. As is usual, if you want to do something very simple in python, the command that you need to do it is also very simple."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "#making the data\n",
    "x = np.linspace(-2, 2, 100)\n",
    "y = x**2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.plot(x, y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You may notice that there is a line of output above the plot. This is because we called a function, but did not assign it to anything. If this line is undesired, we could either assign it to a class attribute: \n",
    "```python\n",
    "fig = plt.plot(x,y)\n",
    "```\n",
    "or we can supress the output by adding a semicolon. This semicolon only needs to be added to the last line of code that creates output.\n",
    "```python\n",
    "plt.plot(x,y);\n",
    "```\n",
    "\n",
    "You may also notice that our data should be a discrete number of points, but instead it is showing up as a line when we plot it. This is because a line is the default. Let's take a closer look at the plot function. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "plt.plot?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The plot function has a number of options. As you will begin to expect in python, your options for the line type are rather intuitive, but also concise, only taking up one or two characters.  \n",
    "\n",
    "| String | Result |  \n",
    "|--------|--------|  \n",
    "|.|Points|  \n",
    "|-|Line|  \n",
    "|--|Dashed Line|  \n",
    "|o|Circles|  \n",
    "|.-|Dot-Dash Line|  \n",
    "|\\*|Star|\n",
    "|x|X|\n",
    "\n",
    "To specify a color, you can add whatever letter seems most intuitive to the beginning of this symbol. Black is 'k'. If you do not specify a color, it will be chosen for you. If this is confusing, you may also explicitly define the color:\n",
    "```python\n",
    "plt.plot(x,y,color='r','.')\n",
    "```\n",
    "Another option is to use **plt.scatter()** to make a scatter plot. This has its own set of options.\n",
    "\n",
    "The plot function typically takes arrays as input. It can also take lists, but it internally converts them to arrays. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "plt.plot(x, y, 'r.');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.plot(x, y, 'g--', linewidth = 2);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plotting in the notebook is particularly nice because you can simply do one plot per cell. As your plots get more complicated, so does the code that creates them, and code cells help to keep everything organized. Simply give multiple plot commands in the same cell to get more than one line on your plots. Alternatively, you may plot them all in the same command, but this can get more confusing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "x = np.linspace(0, 2.5, 12)\n",
    "plt.plot(x, x, 'r-o')\n",
    "plt.plot(x, x**2, 'g-s')\n",
    "plt.plot(x, x**3, 'b-^');\n",
    "#I have added lines to connect the points in this plot to show how it is done"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.plot(x, x, 'ro', x, x**2, 'gs', x, x**3, 'b^');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## More Plotting Options\n",
    "Obviously you want to do more with a plot than simply add points or lines. Let's explore a bunch of basics all at once. Everything added below is farily straightforward. There is a large volume of plotting related functions, so I will not go into detail with each one as I have done with some of the numpy functions.\n",
    "\n",
    "We have added:\n",
    "* A specified size for the plot\n",
    "* X and Y axis labels with designated text size\n",
    "* A title\n",
    "* A legend (In this example I have used LaTeX for the strings, this is not required)\n",
    "* Minor ticks\n",
    "* A log scale Y axis\n",
    "* A grid (for both major and minor ticks)\n",
    "\n",
    "Do not forget that it is easy to look up the documentation for each function in iPython environments. Also, just about and question you may come across while plotting can likely be answered with a quick google search. Notice that the **figure()** function not only creates a new figure, but also can specify a few other parameters. Keep in mind that every function introduced in this notebook has its own set of optional parameters.\n",
    "\n",
    "In this example I let the function decide the best location for the legend, but it can also be specified if it does not seem to be making wise choices."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "plt.figure?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "x = np.linspace(0, 2.5, 16)\n",
    "\n",
    "plt.figure(figsize=(9, 6))\n",
    "plt.plot(x, x, 'ro', label = \"$y = x$\")\n",
    "plt.plot(x, x**2, 'gs', label = \"$y = x^2$\")\n",
    "plt.plot(x, x**3, 'b^', label = \"$y = x^3$\")\n",
    "plt.xlabel('X-values', fontsize = 'large')\n",
    "plt.ylabel('Y-values (log scale)', fontsize = 'large')\n",
    "plt.yscale('log')\n",
    "plt.title('Example Plot With Many Extra Features', fontsize= 'x-large')\n",
    "plt.legend(loc = \"best\")\n",
    "plt.minorticks_on()\n",
    "plt.grid(True, which = 'major', linewidth = 1)\n",
    "plt.grid(which = 'minor', linewidth = .25);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Much of the time python does not always choose the appropriate limits for our plot. Specifying the range for the x and y axis is reasonably simple."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "x = np.linspace(0,2*np.pi,100)\n",
    "y = np.sin(x)\n",
    "y2 = np.cos(x)\n",
    "\n",
    "plt.plot(x, y,'ro', label = ('$y = cos(x)$'))\n",
    "plt.plot(x, y2, 'b^', label = ('$y = sin(x)$'))\n",
    "plt.legend(loc = 'best')\n",
    "plt.xlabel('X (radians)')\n",
    "plt.ylabel('Y')\n",
    "plt.xlim(0, 2*np.pi)\n",
    "plt.ylim(-1.1, 1.1);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "Alternatively, if we want to see all of our points, we could define our limits as follows:\n",
    "```python\n",
    "plt.xlim(x.min()*1.1,x.max()*1.1)\n",
    "ply.ylim(y.min()*1.1,y.max()*1.1)\n",
    "```\n",
    "\n",
    "The ticks can also be explicitly labeled. In this plot, for example, it makes more sense to have the x axis in multiples of pi. While we are at it, let's make all of the text bigger and easier to read.\n",
    "\n",
    "In this example, we use a list to designate the ticks. Arrays can also be used; do not forget that we have some shortcuts to making arrays that might be able to help."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "matplotlib.rc('font', size = 20) #this change is not specific to just this plot\n",
    "plt.figure(figsize = (12, 8))\n",
    "plt.plot(x, y, 'ro', label = ('$y = sin(x)$'))\n",
    "plt.plot(x, y2, 'b^', label = ('$y = cos(x)$'))\n",
    "plt.legend(loc = 'best')\n",
    "plt.xlabel('X (radians)')\n",
    "plt.ylabel('Y')\n",
    "plt.title('sine and cosine')\n",
    "plt.xlim(0, 2 * np.pi)\n",
    "plt.ylim(-1.1, 1.1)\n",
    "plt.xticks([0, np.pi / 2, np.pi, 3 * np.pi / 2, 2 * np.pi],\n",
    "       [r'$0$', r'$\\pi/2$', r'$\\pi$', r'$3\\pi/2$', r'$2\\pi$']);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "Transparency can also be changed by adding the **alpha** parameter. The default is 1, or opaque. Values between zero and one span the range of transparency. This is particularly useful when filling in areas or plotting overlapping points.\n",
    "\n",
    "Another useful tool is the axis object. To use this we need to actually define our plot to be an object when we create the plot and the define the axis object. Here we will use it to simply add a horizontal line, but if you use the **dir()** function on a axis class attribute, you will see that there are a large number instance attributes available. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "matplotlib.rc('font', size = 15)\n",
    "\n",
    "fig = plt.figure(figsize = (9, 6))\n",
    "ax=fig.add_subplot(111) \n",
    "\n",
    "plt.plot(x, y, 'r-', label = ('$y = sin(x)$'), linewidth = 2.5)\n",
    "plt.plot(x, y2, 'b-', label = ('$y = cos(x)$'), linewidth = 2.5)\n",
    "plt.xlim(0, 2 * np.pi)\n",
    "plt.ylim(-1.1, 1.1)\n",
    "plt.legend(loc = 'best')\n",
    "plt.xlabel('X (radians)')\n",
    "plt.ylabel('Y')\n",
    "plt.title('sine and cosine')\n",
    "plt.fill_between(x, y, color = 'red', alpha = .3)\n",
    "plt.fill_between(x, y2, color = 'blue', alpha = .3)\n",
    "plt.xticks([0, np.pi / 2, np.pi, 3 * np.pi / 2, 2 * np.pi],\n",
    "       [r'$0$', r'$\\pi/2$', r'$\\pi$', r'$3\\pi/2$', r'$2\\pi$']);\n",
    "ax.hlines(0, x[0], x[-1]);\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "As you might notice, my code for plotting in python takes up a number of lines, but it also makes it very easy to remove or change between any number of plot additions by simply using `#` to comment out any number of lines."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Bar Charts\n",
    "Another type of plot you may be interested in is a bar chart. For this example we are just going to plot for some caregorical data A-F."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#create our data\n",
    "categories = ['A', 'B', 'C', 'D', 'E', 'F']\n",
    "xpos = np.arange(len(categories))\n",
    "values = 5 + 10*np.random.rand(len(categories))\n",
    "\n",
    "#create the bar chart\n",
    "plt.bar(xpos, values, align = 'center', alpha = .5)\n",
    "plt.xticks(xpos, categories)\n",
    "plt.xlabel('Categories')\n",
    "plt.ylabel('Values')\n",
    "plt.title('Basic Bar Chart');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Histograms\n",
    "There are a large number of statistical plots in the matplotlib gallery, but the most common is a histogram, so here is an example of how to make one of those. Keep in mind that there are always more options, which you may find in the matplotlib gallery, looking at documentation, or with a google search."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#create the data\n",
    "mean = 1000\n",
    "sigma = 100\n",
    "data = mean + sigma*np.random.randn(1000)\n",
    "bins = 20\n",
    "\n",
    "#create the histogram\n",
    "plt.hist(data, bins)\n",
    "plt.title(\"Basic Histogram: $\\mu=1000,\\sigma=100$\")\n",
    "plt.xlabel(\"Value\")\n",
    "plt.ylabel(\"Frequency\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Images\n",
    "There are a number of tools and packages in python for manipulating images. For now, let's just disply some. The first step is to open the image.\n",
    "\n",
    "The image will be read into an array. The array will have three dimensions- two for the image, and one for the color. Any array can be plotted using imshow."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import matplotlib.image as mpimg"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "tipsy = mpimg.imread('tipsy.jpg')\n",
    "print type(tipsy)\n",
    "print tipsy.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize = (9, 6))\n",
    "plt.imshow(tipsy)\n",
    "plt.axis('off')\n",
    "plt.title(\"Tipsy in the grass\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We already know how to manipulate arrays, so it is very easy to do something like reversing the image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "tipsyreverse = tipsy[:,::-1,:]\n",
    "plt.imshow(tipsyreverse);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's try working with a 2-dimensional image. To do this we can just select one of the colors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize = (9, 6))\n",
    "rambo = mpimg.imread('rambo.jpg')\n",
    "plt.imshow(rambo)\n",
    "plt.axis('off');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "rambo1color = rambo[:,:,0]\n",
    "plt.imshow(rambo1color);\n",
    "print rambo1color.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You will notice that even though the array is only 2 dimensional, there is still color in our plot. That is because imshow is using the default color map. Once again there are a number of options for the imshow function. For example, we can change the color map. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.imshow(rambo1color, cmap='hot')\n",
    "plt.colorbar();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The images are stored as an array, so it is also relatively easy to make a quick histogram of the intensity values for each pixel for this color. The range of the intensity in pixels for this image is 256 because there are 8 bits per pixel.\n",
    "\n",
    "The **.ravel()** instance attribute converts any array into a 1-dimensional array by simply concatenating the rows on the end of the first row. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.hist(rambo1color.ravel(), bins=256, range=(0., 256.), fc='k');\n",
    "plt.xlim(0,256);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Subplots\n",
    "You may want to plot more than one thing at a time. This is done using the **subplot()** function. It takes 3 arguments in the form **subplot(nrows,ncolumns,plot_number)**. Use this function before each individual plot by keeping the number of rows and columns constant, but just changing the plot_number. Everything else is done the same way as it usually is. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize = (18, 24))\n",
    "\n",
    "plt.subplot(3,2,1) #first plot- row 1 column 1\n",
    "\n",
    "x = np.linspace(0, 2.5, 16)\n",
    "\n",
    "plt.plot(x, x, 'ro', label = \"$y = x$\")\n",
    "plt.plot(x, x**2, 'gs', label = \"$y = x^2$\")\n",
    "plt.plot(x, x**3, 'b^', label = \"$y = x^3$\")\n",
    "plt.xlabel('X-values', fontsize = 'large')\n",
    "plt.ylabel('Y-values (log scale)', fontsize = 'large')\n",
    "plt.yscale('log')\n",
    "plt.title('Example Plot', fontsize= 'x-large')\n",
    "plt.legend(loc = \"best\")\n",
    "plt.minorticks_on()\n",
    "plt.grid(True, which = 'major', linewidth = 1)\n",
    "plt.grid(which = 'minor', linewidth = .25)\n",
    "\n",
    "plt.subplot(3,2,2) #second plot- row 1 column 2\n",
    "\n",
    "x = np.linspace(0,2*np.pi,100)\n",
    "y = np.sin(x)\n",
    "y2 = np.cos(x)\n",
    "plt.plot(x, y, 'r-', label = ('$y = sin(x)$'), linewidth = 2.5)\n",
    "plt.plot(x, y2, 'b-', label = ('$y = cos(x)$'), linewidth = 2.5)\n",
    "plt.xlim(0, 2 * np.pi)\n",
    "plt.ylim(-1.1, 1.1)\n",
    "plt.legend(loc = 'best')\n",
    "plt.xlabel('X (radians)')\n",
    "plt.ylabel('Y')\n",
    "plt.title('sine and cosine')\n",
    "plt.fill_between(x, y, color = 'red', alpha = .3)\n",
    "plt.fill_between(x, y2, color = 'blue', alpha = .3)\n",
    "plt.xticks([0, np.pi / 2, np.pi, 3 * np.pi / 2, 2 * np.pi],\n",
    "       [r'$0$', r'$\\pi/2$', r'$\\pi$', r'$3\\pi/2$', r'$2\\pi$']);\n",
    "ax.hlines(0, x[0], x[-1])\n",
    "\n",
    "plt.subplot(3,2,3) #third plot- row 2 column 1\n",
    "\n",
    "plt.bar(xpos, values, align = 'center', alpha = .5)\n",
    "plt.xticks(xpos, categories)\n",
    "plt.xlabel('Categories')\n",
    "plt.ylabel('Values')\n",
    "plt.title('Basic Bar Chart')\n",
    "\n",
    "plt.subplot(3,2,4) #fourth plot- row 2 column 2\n",
    "\n",
    "plt.hist(data, bins)\n",
    "plt.title(\"Basic Histogram: $\\mu=1000,\\sigma=100$\")\n",
    "plt.xlabel(\"Value\")\n",
    "plt.ylabel(\"Frequency\")\n",
    "\n",
    "plt.subplot(3,2,5) #fifth plot- row 3 column 1\n",
    "\n",
    "plt.imshow(tipsy)\n",
    "plt.axis('off')\n",
    "\n",
    "plt.subplot(3,2,6) #sixth plot- row 3 column 2\n",
    "\n",
    "plt.imshow(rambo)\n",
    "plt.axis('off');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Saving Plots to Files\n",
    "One way to save the plot in the notebook is to simply right click your plot and save it as an image. However, this might be inconvenient or you might not be using a notebook.\n",
    "\n",
    "To save it as a file, use the **savefig()** instance attribute. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize = (9, 6))\n",
    "ax=fig.add_subplot(111) \n",
    "\n",
    "x = np.linspace(0,2*np.pi,100)\n",
    "y = np.sin(x)\n",
    "y2 = np.cos(x)\n",
    "\n",
    "plt.plot(x, y, 'r-', label = ('$y = sin(x)$'), linewidth = 2.5)\n",
    "plt.plot(x, y2, 'b-', label = ('$y = cos(x)$'), linewidth = 2.5)\n",
    "plt.xlim(0, 2 * np.pi)\n",
    "plt.ylim(-1.1, 1.1)\n",
    "plt.legend(loc = 'best')\n",
    "plt.xlabel('X (radians)')\n",
    "plt.ylabel('Y')\n",
    "plt.title('sine and cosine')\n",
    "plt.fill_between(x, y, color = 'red', alpha = .3)\n",
    "plt.fill_between(x, y2, color = 'blue', alpha = .3)\n",
    "plt.xticks([0, np.pi / 2, np.pi, 3 * np.pi / 2, 2 * np.pi],\n",
    "       [r'$0$', r'$\\pi/2$', r'$\\pi$', r'$3\\pi/2$', r'$2\\pi$']);\n",
    "ax.hlines(0, x[0], x[-1]);\n",
    "\n",
    "fig.savefig(\"plot.png\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
